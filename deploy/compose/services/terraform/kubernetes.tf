output "cluster_name" {
  value = "legion.k8s.local"
}

output "master_security_group_ids" {
  value = ["${aws_security_group.masters-legion-k8s-local.id}"]
}

output "masters_role_arn" {
  value = "${aws_iam_role.masters-legion-k8s-local.arn}"
}

output "masters_role_name" {
  value = "${aws_iam_role.masters-legion-k8s-local.name}"
}

output "node_security_group_ids" {
  value = ["${aws_security_group.nodes-legion-k8s-local.id}"]
}

output "node_subnet_ids" {
  value = ["${aws_subnet.us-east-1a-legion-k8s-local.id}", "${aws_subnet.us-east-1b-legion-k8s-local.id}", "${aws_subnet.us-east-1c-legion-k8s-local.id}"]
}

output "nodes_role_arn" {
  value = "${aws_iam_role.nodes-legion-k8s-local.arn}"
}

output "nodes_role_name" {
  value = "${aws_iam_role.nodes-legion-k8s-local.name}"
}

output "region" {
  value = "us-east-1"
}

output "vpc_id" {
  value = "${aws_vpc.legion-k8s-local.id}"
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_autoscaling_attachment" "master-us-east-1a-masters-legion-k8s-local" {
  elb                    = "${aws_elb.api-legion-k8s-local.id}"
  autoscaling_group_name = "${aws_autoscaling_group.master-us-east-1a-masters-legion-k8s-local.id}"
}

resource "aws_autoscaling_group" "master-us-east-1a-masters-legion-k8s-local" {
  name                 = "master-us-east-1a.masters.legion.k8s.local"
  launch_configuration = "${aws_launch_configuration.master-us-east-1a-masters-legion-k8s-local.id}"
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = ["${aws_subnet.us-east-1a-legion-k8s-local.id}"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "legion.k8s.local"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "master-us-east-1a.masters.legion.k8s.local"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/master"
    value               = "1"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_group" "nodes-legion-k8s-local" {
  name                 = "nodes.legion.k8s.local"
  launch_configuration = "${aws_launch_configuration.nodes-legion-k8s-local.id}"
  max_size             = 3
  min_size             = 3
  vpc_zone_identifier  = ["${aws_subnet.us-east-1a-legion-k8s-local.id}", "${aws_subnet.us-east-1b-legion-k8s-local.id}", "${aws_subnet.us-east-1c-legion-k8s-local.id}"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "legion.k8s.local"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "nodes.legion.k8s.local"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/node"
    value               = "1"
    propagate_at_launch = true
  }
}

resource "aws_ebs_volume" "a-etcd-events-legion-k8s-local" {
  availability_zone = "us-east-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster    = "legion.k8s.local"
    Name                 = "a.etcd-events.legion.k8s.local"
    "k8s.io/etcd/events" = "a/a"
    "k8s.io/role/master" = "1"
  }
}

resource "aws_ebs_volume" "a-etcd-main-legion-k8s-local" {
  availability_zone = "us-east-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster    = "legion.k8s.local"
    Name                 = "a.etcd-main.legion.k8s.local"
    "k8s.io/etcd/main"   = "a/a"
    "k8s.io/role/master" = "1"
  }
}

resource "aws_eip" "us-east-1a-legion-k8s-local" {
  vpc = true
}

resource "aws_eip" "us-east-1b-legion-k8s-local" {
  vpc = true
}

resource "aws_eip" "us-east-1c-legion-k8s-local" {
  vpc = true
}

resource "aws_elb" "api-legion-k8s-local" {
  name = "api-legion-k8s-local-8aik3b"

  listener = {
    instance_port     = 443
    instance_protocol = "TCP"
    lb_port           = 443
    lb_protocol       = "TCP"
  }

  security_groups = ["${aws_security_group.api-elb-legion-k8s-local.id}"]
  subnets         = ["${aws_subnet.utility-us-east-1a-legion-k8s-local.id}", "${aws_subnet.utility-us-east-1b-legion-k8s-local.id}", "${aws_subnet.utility-us-east-1c-legion-k8s-local.id}"]

  health_check = {
    target              = "TCP:443"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 10
    timeout             = 5
  }

  idle_timeout = 300

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "api.legion.k8s.local"
  }
}

resource "aws_iam_instance_profile" "masters-legion-k8s-local" {
  name = "masters.legion.k8s.local"
  role = "${aws_iam_role.masters-legion-k8s-local.name}"
}

resource "aws_iam_instance_profile" "nodes-legion-k8s-local" {
  name = "nodes.legion.k8s.local"
  role = "${aws_iam_role.nodes-legion-k8s-local.name}"
}

resource "aws_iam_role" "masters-legion-k8s-local" {
  name               = "masters.legion.k8s.local"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_masters.legion.k8s.local_policy")}"
}

resource "aws_iam_role" "nodes-legion-k8s-local" {
  name               = "nodes.legion.k8s.local"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_nodes.legion.k8s.local_policy")}"
}

resource "aws_iam_role_policy" "masters-legion-k8s-local" {
  name   = "masters.legion.k8s.local"
  role   = "${aws_iam_role.masters-legion-k8s-local.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_masters.legion.k8s.local_policy")}"
}

resource "aws_iam_role_policy" "nodes-legion-k8s-local" {
  name   = "nodes.legion.k8s.local"
  role   = "${aws_iam_role.nodes-legion-k8s-local.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_nodes.legion.k8s.local_policy")}"
}

resource "aws_internet_gateway" "legion-k8s-local" {
  vpc_id = "${aws_vpc.legion-k8s-local.id}"

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "legion.k8s.local"
  }
}

resource "aws_key_pair" "kubernetes-legion-k8s-local-b2a28842e9bb66b88bf45207d37adf21" {
  key_name   = "kubernetes.legion.k8s.local-b2:a2:88:42:e9:bb:66:b8:8b:f4:52:07:d3:7a:df:21"
  public_key = "${file("${path.module}/data/aws_key_pair_kubernetes.legion.k8s.local-b2a28842e9bb66b88bf45207d37adf21_public_key")}"
}

resource "aws_launch_configuration" "master-us-east-1a-masters-legion-k8s-local" {
  name_prefix                 = "master-us-east-1a.masters.legion.k8s.local-"
  image_id                    = "ami-08431d73"
  instance_type               = "t2.nano"
  key_name                    = "${aws_key_pair.kubernetes-legion-k8s-local-b2a28842e9bb66b88bf45207d37adf21.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.masters-legion-k8s-local.id}"
  security_groups             = ["${aws_security_group.masters-legion-k8s-local.id}"]
  associate_public_ip_address = false
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_master-us-east-1a.masters.legion.k8s.local_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 64
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_launch_configuration" "nodes-legion-k8s-local" {
  name_prefix                 = "nodes.legion.k8s.local-"
  image_id                    = "ami-08431d73"
  instance_type               = "t2.nano"
  key_name                    = "${aws_key_pair.kubernetes-legion-k8s-local-b2a28842e9bb66b88bf45207d37adf21.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.nodes-legion-k8s-local.id}"
  security_groups             = ["${aws_security_group.nodes-legion-k8s-local.id}"]
  associate_public_ip_address = false
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_nodes.legion.k8s.local_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 128
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_nat_gateway" "us-east-1a-legion-k8s-local" {
  allocation_id = "${aws_eip.us-east-1a-legion-k8s-local.id}"
  subnet_id     = "${aws_subnet.utility-us-east-1a-legion-k8s-local.id}"
}

resource "aws_nat_gateway" "us-east-1b-legion-k8s-local" {
  allocation_id = "${aws_eip.us-east-1b-legion-k8s-local.id}"
  subnet_id     = "${aws_subnet.utility-us-east-1b-legion-k8s-local.id}"
}

resource "aws_nat_gateway" "us-east-1c-legion-k8s-local" {
  allocation_id = "${aws_eip.us-east-1c-legion-k8s-local.id}"
  subnet_id     = "${aws_subnet.utility-us-east-1c-legion-k8s-local.id}"
}

resource "aws_route" "0-0-0-0--0" {
  route_table_id         = "${aws_route_table.legion-k8s-local.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.legion-k8s-local.id}"
}

resource "aws_route" "private-us-east-1a-0-0-0-0--0" {
  route_table_id         = "${aws_route_table.private-us-east-1a-legion-k8s-local.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.us-east-1a-legion-k8s-local.id}"
}

resource "aws_route" "private-us-east-1b-0-0-0-0--0" {
  route_table_id         = "${aws_route_table.private-us-east-1b-legion-k8s-local.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.us-east-1b-legion-k8s-local.id}"
}

resource "aws_route" "private-us-east-1c-0-0-0-0--0" {
  route_table_id         = "${aws_route_table.private-us-east-1c-legion-k8s-local.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.us-east-1c-legion-k8s-local.id}"
}

resource "aws_route_table" "legion-k8s-local" {
  vpc_id = "${aws_vpc.legion-k8s-local.id}"

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "legion.k8s.local"
  }
}

resource "aws_route_table" "private-us-east-1a-legion-k8s-local" {
  vpc_id = "${aws_vpc.legion-k8s-local.id}"

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "private-us-east-1a.legion.k8s.local"
  }
}

resource "aws_route_table" "private-us-east-1b-legion-k8s-local" {
  vpc_id = "${aws_vpc.legion-k8s-local.id}"

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "private-us-east-1b.legion.k8s.local"
  }
}

resource "aws_route_table" "private-us-east-1c-legion-k8s-local" {
  vpc_id = "${aws_vpc.legion-k8s-local.id}"

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "private-us-east-1c.legion.k8s.local"
  }
}

resource "aws_route_table_association" "private-us-east-1a-legion-k8s-local" {
  subnet_id      = "${aws_subnet.us-east-1a-legion-k8s-local.id}"
  route_table_id = "${aws_route_table.private-us-east-1a-legion-k8s-local.id}"
}

resource "aws_route_table_association" "private-us-east-1b-legion-k8s-local" {
  subnet_id      = "${aws_subnet.us-east-1b-legion-k8s-local.id}"
  route_table_id = "${aws_route_table.private-us-east-1b-legion-k8s-local.id}"
}

resource "aws_route_table_association" "private-us-east-1c-legion-k8s-local" {
  subnet_id      = "${aws_subnet.us-east-1c-legion-k8s-local.id}"
  route_table_id = "${aws_route_table.private-us-east-1c-legion-k8s-local.id}"
}

resource "aws_route_table_association" "utility-us-east-1a-legion-k8s-local" {
  subnet_id      = "${aws_subnet.utility-us-east-1a-legion-k8s-local.id}"
  route_table_id = "${aws_route_table.legion-k8s-local.id}"
}

resource "aws_route_table_association" "utility-us-east-1b-legion-k8s-local" {
  subnet_id      = "${aws_subnet.utility-us-east-1b-legion-k8s-local.id}"
  route_table_id = "${aws_route_table.legion-k8s-local.id}"
}

resource "aws_route_table_association" "utility-us-east-1c-legion-k8s-local" {
  subnet_id      = "${aws_subnet.utility-us-east-1c-legion-k8s-local.id}"
  route_table_id = "${aws_route_table.legion-k8s-local.id}"
}

resource "aws_security_group" "api-elb-legion-k8s-local" {
  name        = "api-elb.legion.k8s.local"
  vpc_id      = "${aws_vpc.legion-k8s-local.id}"
  description = "Security group for api ELB"

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "api-elb.legion.k8s.local"
  }
}

resource "aws_security_group" "masters-legion-k8s-local" {
  name        = "masters.legion.k8s.local"
  vpc_id      = "${aws_vpc.legion-k8s-local.id}"
  description = "Security group for masters"

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "masters.legion.k8s.local"
  }
}

resource "aws_security_group" "nodes-legion-k8s-local" {
  name        = "nodes.legion.k8s.local"
  vpc_id      = "${aws_vpc.legion-k8s-local.id}"
  description = "Security group for nodes"

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "nodes.legion.k8s.local"
  }
}

resource "aws_security_group_rule" "all-master-to-master" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-legion-k8s-local.id}"
  source_security_group_id = "${aws_security_group.masters-legion-k8s-local.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-master-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes-legion-k8s-local.id}"
  source_security_group_id = "${aws_security_group.masters-legion-k8s-local.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-node-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes-legion-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-legion-k8s-local.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "api-elb-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.api-elb-legion-k8s-local.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "https-api-elb-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.api-elb-legion-k8s-local.id}"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "https-elb-to-master" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-legion-k8s-local.id}"
  source_security_group_id = "${aws_security_group.api-elb-legion-k8s-local.id}"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "master-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.masters-legion-k8s-local.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.nodes-legion-k8s-local.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-to-master-tcp-1-4000" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-legion-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-legion-k8s-local.id}"
  from_port                = 1
  to_port                  = 4000
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-4003-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-legion-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-legion-k8s-local.id}"
  from_port                = 4003
  to_port                  = 65535
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-udp-1-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-legion-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-legion-k8s-local.id}"
  from_port                = 1
  to_port                  = 65535
  protocol                 = "udp"
}

resource "aws_security_group_rule" "ssh-external-to-master-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.masters-legion-k8s-local.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh-external-to-node-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.nodes-legion-k8s-local.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_subnet" "us-east-1a-legion-k8s-local" {
  vpc_id            = "${aws_vpc.legion-k8s-local.id}"
  cidr_block        = "172.20.32.0/19"
  availability_zone = "us-east-1a"

  tags = {
    KubernetesCluster                        = "legion.k8s.local"
    Name                                     = "us-east-1a.legion.k8s.local"
    "kubernetes.io/cluster/legion.k8s.local" = "owned"
  }
}

resource "aws_subnet" "us-east-1b-legion-k8s-local" {
  vpc_id            = "${aws_vpc.legion-k8s-local.id}"
  cidr_block        = "172.20.64.0/19"
  availability_zone = "us-east-1b"

  tags = {
    KubernetesCluster                        = "legion.k8s.local"
    Name                                     = "us-east-1b.legion.k8s.local"
    "kubernetes.io/cluster/legion.k8s.local" = "owned"
  }
}

resource "aws_subnet" "us-east-1c-legion-k8s-local" {
  vpc_id            = "${aws_vpc.legion-k8s-local.id}"
  cidr_block        = "172.20.96.0/19"
  availability_zone = "us-east-1c"

  tags = {
    KubernetesCluster                        = "legion.k8s.local"
    Name                                     = "us-east-1c.legion.k8s.local"
    "kubernetes.io/cluster/legion.k8s.local" = "owned"
  }
}

resource "aws_subnet" "utility-us-east-1a-legion-k8s-local" {
  vpc_id            = "${aws_vpc.legion-k8s-local.id}"
  cidr_block        = "172.20.0.0/22"
  availability_zone = "us-east-1a"

  tags = {
    KubernetesCluster                        = "legion.k8s.local"
    Name                                     = "utility-us-east-1a.legion.k8s.local"
    "kubernetes.io/cluster/legion.k8s.local" = "owned"
  }
}

resource "aws_subnet" "utility-us-east-1b-legion-k8s-local" {
  vpc_id            = "${aws_vpc.legion-k8s-local.id}"
  cidr_block        = "172.20.4.0/22"
  availability_zone = "us-east-1b"

  tags = {
    KubernetesCluster                        = "legion.k8s.local"
    Name                                     = "utility-us-east-1b.legion.k8s.local"
    "kubernetes.io/cluster/legion.k8s.local" = "owned"
  }
}

resource "aws_subnet" "utility-us-east-1c-legion-k8s-local" {
  vpc_id            = "${aws_vpc.legion-k8s-local.id}"
  cidr_block        = "172.20.8.0/22"
  availability_zone = "us-east-1c"

  tags = {
    KubernetesCluster                        = "legion.k8s.local"
    Name                                     = "utility-us-east-1c.legion.k8s.local"
    "kubernetes.io/cluster/legion.k8s.local" = "owned"
  }
}

resource "aws_vpc" "legion-k8s-local" {
  cidr_block           = "172.20.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    KubernetesCluster                        = "legion.k8s.local"
    Name                                     = "legion.k8s.local"
    "kubernetes.io/cluster/legion.k8s.local" = "owned"
  }
}

resource "aws_vpc_dhcp_options" "legion-k8s-local" {
  domain_name         = "ec2.internal"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags = {
    KubernetesCluster = "legion.k8s.local"
    Name              = "legion.k8s.local"
  }
}

resource "aws_vpc_dhcp_options_association" "legion-k8s-local" {
  vpc_id          = "${aws_vpc.legion-k8s-local.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.legion-k8s-local.id}"
}

terraform = {
  required_version = ">= 0.9.3"
}
